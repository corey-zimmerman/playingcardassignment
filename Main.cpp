
// Playing Cards
// Corey Zimmerman

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum class Suit
{
	Hearts,
	Diamonds,
	Clubs,
	Spades
	
};
enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};
struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case Rank::Ace:cout << "Ace" << " of ";
		break;
	case Rank::King:cout << "King" << " of ";
		break;
	case Rank::Queen:cout << "Queen" << " of ";
		break;
	case Rank::Jack:cout << "Jack" << " of ";
		break;
	case Rank::Ten:cout << "Ten" << " of ";
		break;
	case Rank::Nine:cout << "Nine" << " of ";
		break;
	case Rank::Eight:cout << "Eight" << " of ";
		break;
	case Rank::Seven:cout << "Seven" << " of ";
		break;
	case Rank::Six:cout << "Six" << " of ";
		break;
	case Rank::Five:cout << "Five" << " of ";
		break;
	case Rank::Four:cout << "Four" << " of ";
		break;
	case Rank::Three:cout << "Three" << " of ";
		break;
	case Rank::Two:cout << "Two" << " of ";
		break;
	default:
		break;
	}

	switch (card.Suit)
	{
	case Suit::Clubs:cout << "Clubs\n"; 
		break;
	case Suit::Spades:cout << "Spades\n";
		break;
	case Suit::Hearts:cout << "Hearts\n";
		break;
	case Suit::Diamonds:cout << "Diamonds\n";
		break;
	default:
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank >= card2.Rank)
	{
		if (card1.Suit >= card2.Suit)
		{
			return card1;
		}
		return card2;
	}
	else
	{return card2;}
}

int main()
{
	Card c1;
	c1.Rank = Rank::King;
	c1.Suit = Suit::Clubs;

	Card c2;
	c2.Rank = Rank::Ace;
	c2.Suit = Suit::Spades;
	
	PrintCard(c1);
	PrintCard(HighCard(c1, c2));

	(void)_getch();
	return 0;
}